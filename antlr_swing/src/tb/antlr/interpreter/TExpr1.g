tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
import java.util.HashMap;
}

@members {
  HashMap variables = new HashMap();
}

prog    : (e=expr {drukuj ($e.text + " = " + $e.out.toString());})* ;
//prog  : (e=expr {drukuj}});

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) { $out = $e1.out + $e2.out; }
        | ^(PLUS i1=ID e2=expr) { $out = $e2.out + Integer.parseInt(variables.get($i1.text).toString()); }
        | ^(MINUS e1=expr e2=expr) { $out = $e1.out - $e2.out; }
        | ^(MUL   e1=expr e2=expr) { $out = $e1.out * $e2.out; }
        | ^(DIV   e1=expr e2=expr) { $out = $e1.out / $e2.out; }
        | ^(PODST i1=ID   e2=expr) { variables.put($i1.text, $e2.out); $out = $e2.out; }
        | ^(DELETE i1=ID) { variables.remove($i1.text); $out = 0; }
        | MEMORY { System.out.println("There are " + variables.size() + " variables in memory"); $out = 0; }
        | INT                      { $out = getInt($INT.text); }
        ;
